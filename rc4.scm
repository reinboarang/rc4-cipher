(use vector-lib)

(define (vector->hex vec)
    (vector-map (lambda (i n) (number->string n 16)) vec))

(define (vector->string vec)
    (list->string (map integer->char (vector->list vec))))

(define (string->vector str)
    (list->vector (map char->integer (string->list str))))

(define (init-sbox)
    (vector-unfold (lambda (i) i) 256))
    
(define (key-schedule key)
    (let loop ([sbox (init-sbox)]
               [i 0]
               [j 0]
               [key-vec (string->vector key)])
        (set! j (modulo (+ j (vector-ref sbox i) 
                             (vector-ref key-vec (modulo i (vector-length key-vec)))) 256))
        (vector-swap! sbox i j)
        (set! i (+ i 1))
        (if (not (= i 255))
            (loop sbox i j key-vec)
            sbox)))

(define (key-stream key len)
    (let loop ([sbox (key-schedule key)]
               [i 0]
               [j 0]
               [l len]
               [ks #()])
        (set! i (modulo (+ i 1) 256))
        (set! j (modulo (+ j (vector-ref sbox i)) 256))
        (vector-swap! sbox i j)
        (set! l (- l 1))
        (if (not (< l 0))
            (loop sbox i j l (vector-append ks (vector (vector-ref sbox 
                                                                   (modulo (+  (vector-ref sbox i)
                                                                               (vector-ref sbox j)) 256)))))
            ks)))

(define (rc4-crypt key msg)
    (let* ([msg-vec (string->vector msg)]
           [key-vec (key-stream key (vector-length msg-vec))])
          (vector-map (lambda (i n)
                            (bitwise-xor n (vector-ref key-vec i)))
                       msg-vec)))
        